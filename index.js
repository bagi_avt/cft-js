const browserSync = require('browser-sync');

browserSync({
    watch: true,
    server: "app",
    files: ["app/*.html", "app/css/*.css", "app/js/*.js", "app/assets/*.svg"]
});