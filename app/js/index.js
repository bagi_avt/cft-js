import sortItem from './sort-item.js'
import filterParam from './filter.js';
import openMore from './more.js';


let tempData = null
let newList = [];

const xhr = new XMLHttpRequest();
xhr.open('GET', "http://localhost:3000/api/data.json",true);
xhr.send();
xhr.onload = () => {
    tempData = JSON.parse(xhr.response).data
    sortItem(tempData);
    let test = new Promise((resolve, reject) => {})
        test.then(() =>getTemplate('more' ,(item)=>{console.log(item)}))
            .then(() =>getTemplate('basket' ,(item)=>{console.log(item)}))

    getTemplate('basket' ,(item)=>{console.log(item)})
}
function getTemplate(name,fun){
    xhr.open("GET", `http://localhost:3000/pages/${name}/index.html`,true);
    xhr.responseType = "document";
    xhr.send();
    xhr.onload = function() {
        fun(this.responseXML);
    }
}

document.querySelectorAll('form input').forEach(input=>{
    input.addEventListener('click', (e)=>{
        const filterInfo = document.querySelector('#filter-info')
        filterInfo.querySelector('a').addEventListener('click', ()=>{
            sortItem(newList)
        })
        filterInfo.style.display = 'block'
        newList = filterParam(tempData);
        filterInfo.querySelector('span').innerText = `Найдено ${newList.length}`
        filterInfo.style.top = e.clientY+'px';
        filterInfo.style.left = 150 + e.clientX+'px';
    })
})

window.onhashchange = function (){
    xhr.open('GET', "http://localhost:3000/api/data.json",true);
    xhr.send();
    xhr.onload = () => {
        openMore(JSON.parse(xhr.response).data[document.location.hash.replace(/#dog/g,'')]);
    }
}

/**
 *
 * New year theme
 */

const nodeList = document.querySelectorAll('body *');
function randomInteger() {
    return Math.round(0.5 + Math.random() * (nodeList.length));
}
function generateColor() {
    return '#' + Math.floor(Math.random()*16777215).toString(16)
}
// setInterval(() => {
//     elList[randomInteger()].style.backgroundColor = generateColor();
//     elList[randomInteger()].style.backgroundColor = '';
// }, 2000);