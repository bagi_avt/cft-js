export default function sortItem(itemList){
    document.querySelector('.content-list').innerHTML = ''
    itemList.sort(()=> Math.random() - 0.5).forEach((item,index)=>{
            let template = document.querySelector('#item').cloneNode(true)
            template.content.firstElementChild.id = 'dog'.concat(index);
            template.content.firstElementChild.addEventListener('click', (event)=>{
                document.location.hash =  "#dog".concat(index);
            })
            template.content.querySelector('img').src = item.templateImg;
            template.content.querySelector('.nickname').innerHTML = item.templateNick;
            template.content.querySelector('.content-list__price').innerHTML = item.templatePrice;
            return document.querySelector('.content-list').append(template.content)
        }
    )
}