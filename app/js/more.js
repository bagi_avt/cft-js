import {types, features} from "./directory.js";


export default function openMore(data){
    document.querySelectorAll('main').forEach(el => el.style.display='none')
    let template = document.querySelector('#more').cloneNode(true)
    let buttonList = template.content.querySelectorAll('.bread-crumbs a')
    buttonList[buttonList.length-1].innerText =
        template.content.querySelector('.title-characteristics span').innerText =
            template.content.querySelector('h1').innerText = data.templateNick;
    template.content.querySelector('img').src = data.templateImg;
    template.content.querySelector('.types').innerText = data.types.map(type => types.find(item => item.typeId === type).value).join(', ');
    template.content.querySelector('.size').innerText = data.size.toString().concat(' кг');
    template.content.querySelector('.features').innerText = data.features.map(feature => features.find(item => item.featureId === feature).value).join(', ');
    template.content.querySelector('.price').innerText = data.templatePrice;
    document.querySelector('body').innerHTML += template.innerHTML
};


