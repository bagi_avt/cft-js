export const types = [
    {typeId: 1, value: 'Охотничьи'},
    {typeId: 2, value: 'Компаньоны'},
    {typeId: 3, value: 'Декоративные'},
    {typeId: 4, value: 'Служебные'}
    ];

export const features = [
    {featureId: 1, value: 'Отсутствует чувство страха'},
    {featureId: 2, value: 'Мало линяет'},
    {featureId: 3, value: 'Отличное здоровье'},
    {featureId: 4, value: 'Хорошее послушание'},
    {featureId: 5, value: 'Очень преданная'}
    ];
