import {types, features} from "./directory.js";

export default function filterParam(tempData){
    let newList = tempData;
    const paramsFilterByTypes = selectParam("type-of-animal");
    paramsFilterByTypes.length && filterByArray(paramsFilterByTypes, 'types');

    const paramsFilterByFeatures = selectParam("features-of-animals");
    paramsFilterByFeatures.length && filterByArray(paramsFilterByFeatures, 'features');

    const paramsFilterBySize = Array.from(document.querySelectorAll(`form input[name = "size-of-animals"]`)).map(input => +input.value)
    paramsFilterBySize.length && filterByBetween(paramsFilterBySize, 'size')

    function selectParam(typeName){
         return Array.from(document.querySelectorAll(`form input[name = "${typeName}"]:checked`)).map(input => +input.value);
    }

    function filterByArray(param, type){
        newList = newList.filter(data => {
            return param.sort().join('') === data[type].filter(item => param.includes(item)).sort().join('')
        })
    }

    function filterByBetween(param, type){
        newList = newList.filter(data => {
            return data[type] >= param[0] && data[type] <= param[1]
        })
    }
    return newList
}


